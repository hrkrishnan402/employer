

import 'package:employees/models/employees.dart';
import 'package:hive_flutter/adapters.dart';

class DBHelper {
  static final DBHelper _instance = DBHelper._internal();

  factory DBHelper() {
    return _instance;
  }

  DBHelper._internal();

  Future uploadToLocalDb(employees) async {
    var box = await Hive.openBox<EmployeeList>("employees");
    await box.add(employees);
  }

  Future getFromLocalDb() async {
    var box = await Hive.openBox<EmployeeList>("employees");
    var values = box.values.toList();
    var employeeList = values[0];
    return employeeList;
  }

  Future<List<Employees>?> searchEmployees(String val)async{
    var box = await Hive.openBox<EmployeeList>("employees");
    var listOfEmployess = box.values.toList()[0].data;
    var result = listOfEmployess?.where((Employees element) => element.name?.toLowerCase().contains(val) ?? false).toList();
    return result;
  }
}
