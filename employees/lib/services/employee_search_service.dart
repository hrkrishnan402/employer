import 'package:employees/database/db_functions.dart';
import 'package:employees/models/employees.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class EmployeeSearchService extends StateNotifier<List<Employees>>{
  EmployeeSearchService() : super([]);

  Future searchEmployees(String val) async{
    DBHelper dbHelper = DBHelper();
    state = await dbHelper.searchEmployees(val) ?? [];
  }
}