import 'package:dio/dio.dart';
import 'package:employees/database/db_functions.dart';
import 'package:employees/models/employees.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:riverpod/riverpod.dart';

class EmployeeService extends StateNotifier<EmployeeList> {
  EmployeeService() : super(EmployeeList());
  Future fetchEmployees() async {
    Dio dioInstance = Dio();
    DBHelper dbHelper = DBHelper();
    Response response = await dioInstance
        .get("https://www.mocky.io/v2/5d565297300000680030a986");
    EmployeeList employees = EmployeeList.fromJson(response.data);
    var box = await Hive.openBox<EmployeeList>('employees');
    if (box.isEmpty) {
      await dbHelper.uploadToLocalDb(employees);
    }
    state = await dbHelper.getFromLocalDb();
  }

  
}
