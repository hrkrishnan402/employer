import 'package:employees/database/db_functions.dart';
import 'package:employees/models/employees.dart';
import 'package:employees/screens/Homepage/widgets/employee_detail_page.dart';
import 'package:employees/screens/Homepage/search_result.dart';
import 'package:employees/screens/Homepage/widgets/search_bar.dart';
import 'package:employees/services/states.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'widgets/employee_list.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: GestureDetector(
          onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context)=> SearchResultPage())),
          child: const Center(
            child: Text("Search Here",style: TextStyle(
              color: Colors.black,
            ), ),
          )),
      ),
      body: Consumer(builder: (context, watch, _) {
        EmployeeList employeeList = watch(employeesStateProvider);
        List<Employees> employees = employeeList.data ?? [];
        return EmployeeListView(
          employees: employees,
        );
      }),
    );
  }


  @override
  void initState() {
    super.initState();
    context.read(employeesStateProvider.notifier).fetchEmployees();
  }
}
