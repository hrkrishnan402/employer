import 'package:employees/models/employees.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class EmployeeDetailPage extends StatelessWidget {
  final Employees employee;
  const EmployeeDetailPage({Key? key, required this.employee})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Card(
          child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ListTile(
            leading: CircleAvatar(
              backgroundImage: NetworkImage(employee.profileImage ?? ""),
            ),
            title: detailsView("Name", employee.name ?? ""),
          ),
          detailsView("Company Name", employee.company?.name ?? ""),
          detailsView("Email",employee.email ?? ""),
          detailsView("Username",employee.username ?? ""),
          detailsView("Address","${employee.address?.city}, ${employee.address?.street}, ${employee.address?.street}"),
          detailsView("Phone Number",employee.phone ?? ""),
          detailsView("Website", employee.company?.bs ?? ""),
          detailsView("Company Details", employee.company?.catchPhrase ?? "")
        ],
      )),
    );
  }
  Widget detailsView(String hint, String detail){
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(hint, style: TextStyle(color: Colors.grey[400])),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(detail, style:const TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
          ),
        ],
      ),
    );
  }
}
