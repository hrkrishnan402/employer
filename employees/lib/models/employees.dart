import 'package:hive_flutter/hive_flutter.dart';
part 'employees.g.dart';

@HiveType(typeId: 0)
class EmployeeList {
  EmployeeList({
    this.data,
  });

  @HiveField(0)
  List<Employees>? data;

  EmployeeList copyWith({
    List<Employees>? data,
  }) =>
      EmployeeList(
        data: data ?? this.data,
      );

  factory EmployeeList.fromJson(List json) => EmployeeList(
        data: List<Employees>.from(json.map((x) => Employees.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from((data ?? []).map((x) => x.toJson())),
      };
}

@HiveType(typeId: 1)
class Employees {
  Employees({
    this.id,
    this.name,
    this.username,
    this.email,
    this.profileImage,
    this.address,
    this.phone,
    this.website,
    this.company,
  });
  @HiveField(1)
  final int? id;
  @HiveField(2)
  final String? name;
  @HiveField(3)
  final String? username;
  @HiveField(4)
  final String? email;
  @HiveField(5)
  final String? profileImage;
  @HiveField(6)
  final Address? address;
  @HiveField(7)
  final String? phone;
  @HiveField(8)
  final String? website;
  @HiveField(9)
  final Company? company;

  Employees copyWith({
    int? id,
    String? name,
    String? username,
    String? email,
    String? profileImage,
    Address? address,
    String? phone,
    String? website,
    Company? company,
  }) =>
      Employees(
        id: id ?? this.id,
        name: name ?? this.name,
        username: username ?? this.username,
        email: email ?? this.email,
        profileImage: profileImage ?? this.profileImage,
        address: address ?? this.address,
        phone: phone ?? this.phone,
        website: website ?? this.website,
        company: company ?? this.company,
      );

  factory Employees.fromJson(Map<String?, dynamic> json) => Employees(
        id: json["id"],
        name: json["name"],
        username: json["username"],
        email: json["email"],
        profileImage: json["profile_image"] ?? null,
        address: Address.fromJson(json["address"]),
        phone: json["phone"] ?? null,
        website: json["website"],
        company:
            json["company"] == null ? null : Company.fromJson(json["company"]),
      );

  Map<String?, dynamic> toJson() => {
        "id": id,
        "name": name,
        "username": username,
        "email": email,
        "profile_image": profileImage ?? null,
        "address": address?.toJson(),
        "phone": phone ?? null,
        "website": website ?? null,
        "company": company == null ? null : company?.toJson(),
      };
}

@HiveType(typeId: 2)
class Address {
  Address({
    this.street,
    this.suite,
    this.city,
    this.zipcode,
    this.geo,
  });

  final String? street;
  final String? suite;
  final String? city;
  final String? zipcode;
  final Geo? geo;

  Address copyWith({
    @HiveField(10) String? street,
    @HiveField(11) String? suite,
    @HiveField(12) String? city,
    @HiveField(13) String? zipcode,
    @HiveField(14) Geo? geo,
  }) =>
      Address(
        street: street ?? this.street,
        suite: suite ?? this.suite,
        city: city ?? this.city,
        zipcode: zipcode ?? this.zipcode,
        geo: geo ?? this.geo,
      );

  factory Address.fromJson(Map<String, dynamic> json) => Address(
        street: json["street"],
        suite: json["suite"],
        city: json["city"],
        zipcode: json["zipcode"],
        geo: Geo.fromJson(json["geo"]),
      );

  Map<String, dynamic> toJson() => {
        "street": street,
        "suite": suite,
        "city": city,
        "zipcode": zipcode,
        "geo": geo?.toJson(),
      };
}

@HiveType(typeId: 3)
class Geo {
  Geo({
    this.lat,
    this.lng,
  });
  @HiveField(15)
  final String? lat;
  @HiveField(16)
  final String? lng;

  Geo copyWith({
    String? lat,
    String? lng,
  }) =>
      Geo(
        lat: lat ?? this.lat,
        lng: lng ?? this.lng,
      );

  factory Geo.fromJson(Map<String, dynamic> json) => Geo(
        lat: json["lat"],
        lng: json["lng"],
      );

  Map<String, dynamic> toJson() => {
        "lat": lat,
        "lng": lng,
      };
}

@HiveType(typeId: 4)
class Company {
  Company({
    this.name,
    this.catchPhrase,
    this.bs,
  });
  @HiveField(17)
  final String? name;
  @HiveField(18)
  final String? catchPhrase;
  @HiveField(19)
  final String? bs;

  Company copyWith({
    String? name,
    String? catchPhrase,
    String? bs,
  }) =>
      Company(
        name: name ?? this.name,
        catchPhrase: catchPhrase ?? this.catchPhrase,
        bs: bs ?? this.bs,
      );

  factory Company.fromJson(Map<String, dynamic> json) => Company(
        name: json["name"],
        catchPhrase: json["catchPhrase"],
        bs: json["bs"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "catchPhrase": catchPhrase,
        "bs": bs,
      };
}
