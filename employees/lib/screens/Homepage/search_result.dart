import 'package:employees/screens/Homepage/widgets/employee_list.dart';
import 'package:employees/screens/Homepage/widgets/search_bar.dart';
import 'package:employees/services/states.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class SearchResultPage extends StatelessWidget {
  SearchResultPage({Key? key}) : super(key: key);
  TextEditingController controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, watch, _) {
      var result = watch(employeesSearchProvider);
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: SearchBar(
            controller: controller,
            onchanged: (val) => context
                .read(employeesSearchProvider.notifier)
                .searchEmployees(val),
          ),
        ),
        body: EmployeeListView(
          employees: result,
        ),
      );
    });
  }
}
