import 'package:employees/models/employees.dart';
import 'package:employees/screens/screens.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:path_provider/path_provider.dart';

void main() async {
  await Hive.initFlutter();
  if (!Hive.isAdapterRegistered(1)) {
    final appDocumentDir = await getApplicationDocumentsDirectory();

    Hive
      ..init(appDocumentDir.path)
      ..registerAdapter(EmployeeListAdapter());
    Hive
      ..init(appDocumentDir.path)
      ..registerAdapter(EmployeesAdapter());
    Hive
      ..init(appDocumentDir.path)
      ..registerAdapter(AddressAdapter());
    Hive
      ..init(appDocumentDir.path)
      ..registerAdapter(GeoAdapter());
    Hive
      ..init(appDocumentDir.path)
      ..registerAdapter(CompanyAdapter());
  }
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const ProviderScope(child: MaterialApp(home: HomePage())));
}
