import 'package:employees/models/employees.dart';
import 'package:employees/services/employee_search_service.dart';
import 'package:riverpod/riverpod.dart';

import 'employee_serivce.dart';

final employeesStateProvider =
    StateNotifierProvider<EmployeeService, EmployeeList>(
        (ref) => EmployeeService());
final employeesSearchProvider =
    StateNotifierProvider<EmployeeSearchService, List<Employees>>(
        (ref) => EmployeeSearchService());
