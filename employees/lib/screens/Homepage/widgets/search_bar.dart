import 'package:employees/database/db_functions.dart';
import 'package:employees/models/employees.dart';
import 'package:employees/screens/Homepage/widgets/employee_list.dart';
import 'package:flutter/material.dart';

class SearchBar extends StatelessWidget {
  final Function(String)? onchanged;
   SearchBar({ Key? key, this.onchanged,  this.controller }) : super(key: key);
  TextEditingController? controller;
  @override
  Widget build(BuildContext context) {
  return ListTile(
      trailing: const Icon(
        Icons.search,
        color: Colors.blue,
        size: 28,
      ),
      title: TextField(
        onChanged: (value) => onchanged?.call(value),
        controller: controller,
        decoration: const InputDecoration(
          hintText: 'type in employee name or email...',
          hintStyle: TextStyle(
            color: Colors.black,
            fontSize: 18,
            fontStyle: FontStyle.italic,
          ),
          border: InputBorder.none,
        ),
        style: const TextStyle(
          color: Colors.black,
        ),
      ),
    );
  }
}