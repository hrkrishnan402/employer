import 'package:employees/models/employees.dart';
import 'package:employees/screens/Homepage/widgets/employee_detail_page.dart';
import 'package:flutter/material.dart';

class EmployeeListView extends StatelessWidget {
  final List<Employees> employees;
  const EmployeeListView({ Key? key,required this.employees }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
              itemCount: employees.length,
              itemBuilder: (context, index) => ListTile(
                onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => EmployeeDetailPage(employee: employees[index]))),
              leading: CircleAvatar(backgroundImage: NetworkImage(employees[index].profileImage ?? ""),),
              title: Text(employees[index].name ?? ""),
              trailing: Text(employees[index].company?.name ?? ""),
            ));
  }
}